require 'sinatra'
require 'json'

class Anadict
  def initialize(dict)
   @wordlist = File.open(dict).read.split("\n")
   @wordmap = Hash[@wordlist.map { |x| [x.downcase, x.chars.to_a.sort.join.downcase] }]
   @wordmap = @wordmap.group_by { |key, value| @wordmap[key] }
   @wordmap.each_pair do |key, value|
    @wordmap[key] = value.transpose.delete_at(0)
   end
  end

  def anagram(word)
    if @wordmap [word.chars.to_a.sort.join.downcase] == nil
       json_state  = {:answer => "#{:word} has no anagrams in the dictionary."}
       json_state.to_json
    else
       json_state = {:answer => "#{@wordmap[word.chars.to_a.sort.join.downcase].sort_by{|k,v| v}.reverse.join(', ')}"}
       json_state.to_json
    end
  end
end


a = Anadict.new("words")


set :environment, :production

post '/v1/word' do
   jdata = JSON.parse(request.body.read) 
   puts jdata["word"]
   a.anagram(jdata["word"])
end

get '/' do
  erb :form
end

get '/v1/word' do
  erb :form
end

get '/v1/status' do
  erb :status
end

get '/:word' do
    a.anagram("#{params[:word]}")
end

post '/' do
    a.anagram("#{params[:word]}")
end

