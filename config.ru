require File.absolute_path("anagram.rb")

set :environment, ENV['RACK_ENV'].to_sym
set :app_file,     'anagram.rb'
disable :run

#log = File.absolute_path("logs/sinatra.log", "a")
#STDOUT.reopen(log)
#STDERR.reopen(log)

run Sinatra::Application
