require 'json'
require 'rest-client'
 
class Anaclient
  attr_reader :word
 
  def initialize word
    @word = word
  end
 
  def post_word
    response = RestClient.post 'http://54.165.229.141/v1/word', :data => {word: @word}.to_json, :accept => :json
    JSON.parse(response,:symbolize_names => true)
  end
end 
