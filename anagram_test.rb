
ENV['RACK_ENV'] = 'test'

require './anagram.rb'
require 'minitest/autorun'
require 'rack/test'

class AnagramTest < Minitest::Test
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def test_it_hello_nurse
    get '/'
    assert last_response.ok?
    assert last_response.body.include?('Welcome')
  end

  def test_it_booya_grandma
    get '/word'
    assert last_response.ok?
    assert last_response.body.include?('{"answer":"word, drow"}')
  end

  def test_it_booya_granddad
    post '/v1/word', '{"word":"word"}'
    assert_equal 200, last_response.status
    json = JSON.parse(last_response.body).fetch('answer')
    puts json
    assert last_response.body.include?('{"answer":"word, drow"}')
  end
end
